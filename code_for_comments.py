import copy
import matplotlib.pyplot as plt
import numpy as np
from imageio import imread, imsave
from skimage.color import rgb2gray
from skimage.filters import try_all_threshold
from skimage.filters import roberts, sobel, scharr, prewitt
from skimage.morphology import binary_closing, binary_opening
from skimage.transform import (hough_line, hough_line_peaks, probabilistic_hough_line)
from skimage.feature import canny
from skimage.feature import corner_fast, corner_peaks, corner_foerstner, corner_shi_tomasi
from scipy.ndimage.morphology import binary_fill_holes
from matplotlib import patches
import cv2

#############################################################
def resize_image(input_image_path, output_image_path,
                 size):
    from PIL import Image
    original_image = Image.open(input_image_path)
    resized_image = original_image.resize(size)
    resized_image.save(output_image_path)
    return resized_image
#############################################################
# constants
IMAGE_HEIGHT = 1600
IMAGE_WIDTH = 900

directory = "data/train/"
images_yes = []
images_no  = []

def read_images(label, num_images, storage):
    for i in range(num_images):
        path = directory + label + str(i + 1) + ".jpg"
        resize_image(path, path, (IMAGE_WIDTH, IMAGE_HEIGHT))
        storage.append(imread(path))
        
def show_images(storage, num_rows, num_images_in_row):
    fig, ax = plt.subplots(num_rows, num_images_in_row, figsize=(15, 10))
    for i in range(num_rows * num_images_in_row):
        ax.flatten()[i].set_title(i)
        ax.flatten()[i].imshow(storage[i])
    plt.show()

read_images("yes/", 5, images_yes)
read_images("no/", 5, images_no)

show_images(images_yes, 1, 5)
show_images(images_no, 1, 5)

image = images_yes[4]

#############################################################
directory = "data/templates/chair/"
single_chair = []

read_images("", 35, single_chair)
show_images(single_chair, 7, 5)

chair = single_chair[6]
#############################################################
chair_gray = rgb2gray(chair)

chair_edges = canny(chair_gray, sigma=3)

f, ax = plt.subplots(1, 2, figsize=(15,15))
ax[0].set_title("Chair")
ax[1].set_title("Edges")
ax[0].imshow(chair)
ax[1].imshow(chair_edges, cmap="gray")
#############################################################
img_gray = rgb2gray(image)

img_edges = canny(img_gray, sigma=2.3)

f, ax = plt.subplots(1, 2, figsize=(15,15))
ax[0].set_title("Original")
ax[1].set_title("Edges")
ax[0].imshow(image)
ax[1].imshow(img_edges, cmap="gray")
ax[1].imshow(chair_edges, cmap="gray")
#############################################################
def show_hough_transform(image, angles):
    gray = rgb2gray(image)
    h, theta, d = hough_line(canny(gray), angles)  # вычисляем преобразование Хаффа от границ изображения

    fig, ax = plt.subplots(1, 1, figsize=(15, 6))

    ax.imshow(image, cmap='gray')
    for _, angle, dist in zip(*hough_line_peaks(h, theta, d, num_peaks=14)):
        y0 = (dist - 0 * np.cos(angle)) / np.sin(angle)
        y1 = (dist - image.shape[1] * np.cos(angle)) / np.sin(angle)
        ax.plot((0, image.shape[1]), (y0, y1), '-r')
    ax.set_xlim((0, image.shape[1]))
    ax.set_ylim((image.shape[0], 0))
    ax.set_axis_off()
    ax.set_title('Detected lines')

    plt.tight_layout()
    plt.show()
    pass

tested_angles = np.linspace(-np.pi/2, np.pi/2, 180)
show_hough_transform(chair, tested_angles)
show_hough_transform(images_yes[0], tested_angles)
show_hough_transform(images_no[0], tested_angles)

#############################################################

def hsv_threshold(image_to_filter):
    low = (106, 77, 0)
    high = (255, 167, 144)
    hsv = cv2.cvtColor(image_to_filter, cv2.COLOR_BGR2HSV)
    thresh = cv2.inRange(hsv, low, high)
    mask = thresh != 0
    ax.flatten()[0].imshow(mask)
    mask = binary_closing(mask, selem=np.ones((30, 30)))
    mask = binary_fill_holes(mask)
    mask = binary_opening(mask, selem=np.ones((30, 30)))
    mask = binary_closing(mask, selem=np.ones((15, 15)))
    mask = binary_closing(mask, selem=np.ones((5, 5)))
    res = image_to_filter * (mask[:, :, None].astype(image_to_filter.dtype))
    return res


fig, ax = plt.subplots(1,2, figsize = (30,20))
res = hsv_threshold(image)
ax.flatten()[1].imshow(res)
plt.show()
fig, ax = plt.subplots(1, 4, figsize = (25,16))
detect_corners(res, "harris", 0)
detect_corners(res, "fast", 1)
detect_corners(res, "shitomasi", 2)
detect_corners(res, "foerstner", 3)
plt.show()

#############################################################

# Метод для получения бинаризованного изображения
# Применяем морфологические операции, чтобы после бинаризации на изображении остался только стул 
def make_object_mask(image):
    fig, ax = plt.subplots(2, 2, figsize=(15, 6))
    my_edge_segmentation = binary_closing(canny(image, sigma=1.5), selem=np.ones((3, 3)))
    my_edge_segmentation = binary_fill_holes(my_edge_segmentation)
    ax.flatten()[0].imshow(my_edge_segmentation)
    my_edge_segmentation = binary_opening(my_edge_segmentation, selem=np.ones((3, 3)))
    ax.flatten()[1].imshow(my_edge_segmentation)
    my_edge_segmentation = binary_closing(my_edge_segmentation, selem=np.ones((4, 4)))
    ax.flatten()[2].imshow(my_edge_segmentation)
    my_edge_segmentation = binary_fill_holes(my_edge_segmentation)
    my_edge_segmentation = binary_opening(my_edge_segmentation, selem=np.ones((4, 4)))
    ax.flatten()[3].imshow(my_edge_segmentation)
    plt.title('binarization: after opening', pad = 10)
    plt.tight_layout()
    plt.show()
    return my_edge_segmentation


# Метод для вычисления границ описывающего прямоугольника
# Используется результат бинаризации, по которому находятся крайние точки стула
def make_object_border(image):
    from matplotlib.patches import Rectangle
    mask = make_object_mask(image)
    vertical_indices = np.where(np.any(mask, axis=1))[0]
    top = vertical_indices[0]
    bottom = vertical_indices[-1]
    horizontal_indices = np.where(np.any(mask, axis=0))[0]
    left = horizontal_indices[0]
    right = horizontal_indices[-1]
    rect_border = Rectangle((left, top), right - left, bottom - top, linewidth=4, edgecolor='g', facecolor='none')
    return rect_border


# Метод для получения обрезанного изображения по исходному изображению и вычисленным границам
def crop_image(img, border):
    return img[border[0] : border[1], border[2] : border[3]]

#############################################################

plt.imshow(chair)
plt.title('original template image', pad = 10)
plt.show()

chair_gray = rgb2gray(chair)
border = make_object_border(chair_gray)
fig, ax = plt.subplots(1, 1, figsize=(15, 6))
ax.imshow(chair_gray, cmap='gray')
ax.add_patch(border)
plt.title('bordered chair template', pad = 10)
plt.show()

chair_border = [border.get_y(),
                border.get_y() + border.get_height(),
                border.get_x(),
                border.get_x() + border.get_width()]

selected_chair = crop_image(chair, chair_border)

plt.imshow(selected_chair)
plt.title('finely cropped chair template', pad = 10)
plt.show()

#############################################################

# Метод для рисования в строку двух изображений: 'object' и 'composition'
def draw_object_and_composition(obj, compos, figure_size):
    fig, ax = plt.subplots(1, 2, figsize=figure_size)
    ax[0].set_title('object')
    ax[0].imshow(obj, cmap='gray')
    ax[1].set_title('composition')
    ax[1].imshow(compos, cmap='gray')
    return fig, ax

# Переопределение метода make_object_mask из предыдущей ячейки. Скорее всего, это не очень хорошо. Нужно подумать, как написать по-другому.
# Например, передавать метод 'mask_method' в make_object_border в зависимости от того, какая маска нужна.
def make_object_mask(img):
    my_edge_map = binary_closing(canny(img, sigma=2.3), selem=np.ones((3, 3)))
    my_edge_segmentation = binary_fill_holes(my_edge_map)
    my_edge_segmentation = binary_opening(my_edge_segmentation, selem=np.ones((3, 3)))
    return my_edge_segmentation

#############################################################

# selected_chair получается в предыдущей ячейке
template = cv2.cvtColor(selected_chair, cv2.COLOR_BGR2GRAY)
composition = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

orb_detector = cv2.ORB_create(nfeatures=100000)
kp1, des1 = orb_detector.detectAndCompute(template, None)
kp2, des2 = orb_detector.detectAndCompute(composition, None)

matcher = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
matches = matcher.match(des1, des2)
matches = sorted(matches, key=lambda x:x.distance)

good_matches = []

for m in matches:
    if m.distance < 40:
        good_matches.append(m)

m_len = len(good_matches)
p1 = np.zeros((m_len, 2))
p2 = np.zeros((m_len, 2))
for i in range(m_len):
    p1[i, :] = kp1[good_matches[i].queryIdx].pt
    p2[i, :] = kp2[good_matches[i].trainIdx].pt

img_matches = cv2.drawMatches(template, kp1, composition, kp2, good_matches, None, 
                              flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)

plt.figure(figsize=(15, 15))
plt.imshow(img_matches, interpolation='nearest')
plt.show()
    
fig, ax = draw_object_and_composition(template, composition, figure_size=(15,6))
ax[0].plot(p1[:, 0], p1[:, 1], "o")
ax[1].plot(p2[:, 0], p2[:, 1], "o")

homography, mask = cv2.findHomography(p1, p2, cv2.RANSAC)

h, w = composition.shape
transformed = cv2.warpPerspective(template, homography, (w, h))

fig, ax = draw_object_and_composition(template, composition, figure_size=(15,6))

ax[0].axis('off')
border = make_object_border(template)
ax[0].add_patch(border)

ax[1].axis('off')
border = make_object_border(transformed)
ax[1].add_patch(border)

#############################################################
selected_lines = []
vertical_line_angle_threshold = np.pi / 50.0
door_max_left_border = IMAGE_WIDTH * 0.40
door_min_left_border = IMAGE_WIDTH * 0.10
door_max_right_border = IMAGE_WIDTH * 0.90
door_min_right_border = IMAGE_WIDTH * 0.60
door_default_left_border = IMAGE_WIDTH * 0.32
door_default_right_border = IMAGE_WIDTH * 0.72


def border_intersection_x(theta_rad: float, rho: float):
    """
    Returns 'x' coordinate for intersections with upper and lower borders of image  
    """
    eps = 1e-6
    y0 = (rho - 0 * np.cos(theta_rad)) / np.sin(theta_rad)
    y1 = (rho - IMAGE_WIDTH * np.cos(theta_rad)) / np.sin(theta_rad)
    if (abs(y1 - y0) < eps):
        x = rho
    else:
        x = (((IMAGE_HEIGHT/2 - y0) * IMAGE_WIDTH) / (y1 - y0))
    return x, y0, y1

def select_line(lines):
    res = None
    num_lines = len(lines)
    for i, line in enumerate(lines):
        x = line[0]
        # print(x)
        if (i == num_lines - 1):
            break
        if (lines[i + 1][0] - x > 60/num_lines):
            res = line
            break;
    return res
    

def get_lines(theta_arr, d_arr):
    # find one pair of vertical lines 'x' coordinates if possible
    vertical_lines = [(theta_i, d_i) 
                      for (theta_i, d_i) in zip(theta_arr, d_arr)
                          if (abs(abs(theta_i)) < vertical_line_angle_threshold) 
                     ]
    get_x = lambda pair: border_intersection_x(pair[0], pair[1]) 
    cartesian_v_lines = [get_x((theta_i, d_i)) for (theta_i, d_i) in vertical_lines]
    left_lines = []
    right_lines = []
    for line in cartesian_v_lines:
        if (line[0] > door_min_left_border and line[0] < door_max_left_border):
            left_lines.append(line)
    for line in cartesian_v_lines:
        if (line[0] > door_min_right_border and line[0] < door_max_right_border):
            right_lines.append(line)
    if len(left_lines) == 0 or len(right_lines) == 0:
        return None; # default value
    left_lines = sorted(left_lines, key = lambda x : x[0]) 
    left = select_line(left_lines)
    if (left is None):
        left = left_lines[-1]
    right_lines = sorted(right_lines, key = lambda x : -x[0])     
    right = select_line(right_lines)
    if (right is None):
        right = right_lines[0]
        
    return (left, right)
    
    
def calculate_door_width(theta, d):
    pair = get_lines(theta, d)
    if (pair is None):
        return (None,None)
    return (pair[1][0] - pair[0][0], pair)


def find_door_width(image, draw_plot):
    gray = rgb2gray(image)
    if draw_plot:
        fig, ax = plt.subplots(1, 2, figsize=(15, 10))
        ax[0].imshow(gray, cmap="gray")
        ax[0].set_title('Image')
        ax[0].set_axis_off()
        
    h, angle, dist = hough_line(canny(gray))  # вычисляем преобразование Хаффа от границ изображения
    _, theta, d = hough_line_peaks(h, angle, dist)
    width, coords = calculate_door_width(theta, d)
    if draw_plot:
        ax[1].imshow(gray, cmap="gray")
        for angle, dist in zip(*(theta, d)):
            y0 = (dist - 0 * np.cos(angle)) / np.sin(angle)
            y1 = (dist - gray.shape[1] * np.cos(angle)) / np.sin(angle)
            ax[1].plot((0, gray.shape[1]), (y0, y1), '-r')
        if (coords is not None):
            ax[1].plot((0, gray.shape[1]), (coords[0][1], coords[0][2]), '-g', linewidth=4)
            ax[1].plot((0, gray.shape[1]), (coords[1][1], coords[1][2]), '-g', linewidth=4)
        else:
            ax[1].axvline(door_default_left_border)
            ax[1].axvline(door_default_right_border)
        ax[1].set_xlim((0, gray.shape[1]))
        ax[1].set_ylim((gray.shape[0], 0))
        ax[1].set_axis_off()
        ax[1].set_title('Detected lines')
        plt.tight_layout()
        plt.show()
    if (width is None):
        return (0.72 - 0.32)
    return width / IMAGE_WIDTH 

#############################################################

door_widths = [] # width for 'yes' and 'no' images sequentially 
draw_plot = True
for i in range(len(images_yes)): 
    door_widths.append(find_door_width(images_yes[i], draw_plot))
for i in range(len(images_no)): 
    door_widths.append(find_door_width(images_no[i], draw_plot))
    
print(door_widths)

#############################################################

def find_each_door_width(label, sample_size, output_graphics=False, output_each_test=False):
    width_list = []
    for n in range(0, sample_size):
        filename = str(n + 1) + ".jpg"
        path = directory + label + filename
        resize_image(path, path, (IMAGE_WIDTH, IMAGE_HEIGHT))
        img = rgb2gray(imread(path))
        w = find_door_width(img, draw_plot=output_graphics)
        if output_each_test:
            print("test №", n+1)
            print("Ширина двери: ", w)
        width_list.append(w)
        
    return width_list

#############################################################

chair_top_border = IMAGE_HEIGHT * 0.45
chair_bot_border = IMAGE_HEIGHT * 0.85
chair_left_border = IMAGE_WIDTH * 0.05
chair_right_border = IMAGE_WIDTH * 0.95

def find_chair_width(best_matches, kp_best):
    
    # в массиве best_matchers лежат уже отфильтрованные точки, по которым можно делать выводы о размерах стула
    x_list = []
    for m in best_matches: 
        (x, y) = kp_best[m.trainIdx].pt  # получили координаты точки m из массива keypoints (kp_best) 
        x_list.append(x)
    x_list.sort()
    x_min = x_list[0]
    x_max = x_list[-1]
    
    w = (x_max - x_min) / IMAGE_WIDTH # ширина стула относительно ширины изображения
    return w


def filter_kp(kp, des):
    points = zip(kp, des)
    #points = sorted(points, key = lambda p : p[0].pt[1] ) # sort by 'y' coordinate of key points
    filtered_idx = []
    # Стул на изображениях выборки находится в нижней части (ниже середины изображения, и очень редко выступает за неё)
    # Поскольку данный стул достаточно высокий, его отношение к высоте изображения берется равным 0.5
    # Вторая отметка - 0.85 высоты изображения, убираем точки на ковре. 
    # Также отсекаем 5% с обоих краёв изображения, стул редко туда попадает. 
    for i, p in enumerate(points):
        if (p[0].pt[1] > chair_top_border and p[0].pt[1] < chair_bot_border and 
            p[0].pt[0] > chair_left_border and p[0].pt[0] < chair_right_border):
            filtered_idx.append(i)
    return filtered_idx

#############################################################

def process_all_templates(composition, figsize, threshold, num_templates, 
                      templates_path, output_text=False, output_graphics=True):
    import math
    max_good_matches = 0
    best_path = ""
    best_matches = []
    kp_src_best = []
    kp_dst_best = []
    
    for i in range(1, num_templates + 1):
        filename = str(i) + ".jpg"
        temp_path = templates_path + filename
       
        template = cv2.cvtColor(cv2.imread(temp_path), cv2.COLOR_BGR2GRAY)
        template = cv2.blur(template, (4,4))
        orb_detector = cv2.ORB_create(nfeatures=10000)
        kp1, des1 = orb_detector.detectAndCompute(template, None)
        kp2, des2 = orb_detector.detectAndCompute(composition, None)
        
        remain_idx = filter_kp(kp2, des2)
        
        matcher = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
        matches = matcher.match(des1, des2)
        matches = sorted(matches, key=lambda x:x.distance)
        filtered_matches = []
        good_matches = []
        good_matches_total = 0
        
        for m in matches:
            if (m.trainIdx in remain_idx):
                filtered_matches.append(m)
                
        for m in filtered_matches:
            if m.distance <= threshold:
                good_matches.append(m)
                good_matches_total += 1
        
        n = len(good_matches)
        
        if output_text:
            print("chair template %d, matches: %d" % (i, good_matches_total))
    
        if good_matches_total > max_good_matches:
            max_good_matches = good_matches_total
            best_matches = good_matches
            best_path = temp_path
            kp1_best = kp1
            kp2_best = kp2
            
    template = cv2.cvtColor(cv2.imread(best_path), cv2.COLOR_BGR2GRAY)
    img_matches = cv2.drawMatches(template, kp1_best, composition, kp2_best, best_matches, 
                                  None, flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)
        
    if output_graphics:
        plt.figure(figsize=figsize)
        plt.imshow(img_matches, interpolation='nearest')
        plt.show()
        
    if output_text:   
        print("Best template is: ", best_path)
    
    return find_chair_width(best_matches, kp2_best)

#############################################################

path = 'data/train/yes/6.jpg'
directory_templates = 'data/templates/chair/'
resize_image(path, path, (IMAGE_WIDTH, IMAGE_HEIGHT))
composition = cv2.imread(path)
GOOD_MATCH_THRESHOLD = 40  
NUM_TEMPLATES = 35

w = process_all_templates(composition, output_text=True, output_graphics=True,
                      figsize=(15, 15), threshold=GOOD_MATCH_THRESHOLD, num_templates = NUM_TEMPLATES,
                      templates_path=directory_templates)

print("chair width: ", w)

#############################################################

def find_each_chair_width(directory_name, label, sample_size, output_graphics=False, output_each_test=False):
    width_list = []
    for n in range(0, sample_size):
        filename = str(n + 1) + ".jpg"
        path = directory_name + label + filename
        resize_image(path, path, (IMAGE_WIDTH, IMAGE_HEIGHT))
        composition = cv2.cvtColor(cv2.imread(path), cv2.COLOR_BGR2GRAY)
        composition = cv2.blur(composition, (2,2))
        
    
        w = process_all_templates(composition, output_graphics=output_graphics, output_text=False, 
                              figsize=(9, 9), threshold=GOOD_MATCH_THRESHOLD, num_templates=NUM_TEMPLATES, 
                              templates_path=directory_templates)
        if output_each_test: 
            print("test №", n+1)
            print("Ширина стула: ", w)
        width_list.append(w)
        
    return width_list

#############################################################

directory = "data/train/"
label = "yes/"
sample_size = 5 
width_list = find_each_chair_width(directory, label, sample_size, output_graphics=True, output_each_test = True)
print(width_list)

#############################################################

directory = "data/train/"
label = "no/"
sample_size = 5 
width_list = find_each_chair_width(directory, label, sample_size, output_graphics=True, output_each_test = True)
print(width_list)

#############################################################

def almost_final_algorithm(directory, label, sample_size, threshold, templates_path, num_templates):    
    correct_answers = 0
    
    doors_width = find_each_door_width(directory, label, sample_size)
    chairs_width = find_each_chair_width(directory, label, sample_size)
    pred_labels = []
    
    for i in range(0, len(chairs_width)):
        print("Тест №", i)
        print("Ширина двери: ", doors_width[i], "; Ширина стула: ", chairs_width[i])
        if doors_width[i] > chairs_width[i]:
            if (label == "yes/"):
                pred_labels.append(True)
            else:
                pred_labels.append(False)
        else: 
            if (label == "no/"):
                pred_labels.append(True)
            else:
                pred_labels.append(False)
                
    print(pred_labels)
    correct_answers = 0
    for ans in pred_labels:
        if ans == True:
            correct_answers += 1
    
    return correct_answers

#############################################################

sample_size_yes = 20
sample_size_no = 15
GOOD_MATCH_THRESHOLD = 40
NUM_TEMPLATES = 35
images_directory = "data/train/"
templates_directory = "data/templates/chair/"


correct_ans_yes = almost_final_algorithm(directory=images_directory, label="yes/", sample_size=sample_size_yes, threshold=GOOD_MATCH_THRESHOLD, 
                            templates_path=templates_directory, num_templates=NUM_TEMPLATES)

correct_ans_no = almost_final_algorithm(directory=images_directory, label="no/", sample_size=sample_size_no, threshold=GOOD_MATCH_THRESHOLD, 
                           templates_path=templates_directory, num_templates=NUM_TEMPLATES)

accuracy = (correct_ans_no + correct_ans_yes) / (sample_size_no + sample_size_yes)
print("accuracy = ", accuracy)

#############################################################