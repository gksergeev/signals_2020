import cv2
import numpy as np
from imageio import imread, imsave

def color_trackbars(global_image):
    def nothing(*arg):
        pass

    cv2.namedWindow("result", cv2.WINDOW_AUTOSIZE)  # создаем главное окно
    cv2.namedWindow("settings")  # создаем окно настроек

    cv2.createTrackbar('h1', 'settings', 0, 255, nothing)
    cv2.createTrackbar('s1', 'settings', 0, 255, nothing)
    cv2.createTrackbar('v1', 'settings', 0, 255, nothing)
    cv2.createTrackbar('h2', 'settings', 255, 255, nothing)
    cv2.createTrackbar('s2', 'settings', 255, 255, nothing)
    cv2.createTrackbar('v2', 'settings', 255, 255, nothing)
    crange = [0, 0, 0, 0, 0, 0]
    ratio = 1.5
    kernel_size = 5
    low_threshold = 0
    gray_image = cv2.cvtColor(global_image, cv2.COLOR_BGR2GRAY)
    img_blur = cv2.blur(gray_image, (5, 5))
    detected_edges = cv2.Canny(img_blur, low_threshold, low_threshold * ratio, kernel_size)
    mask = detected_edges != 0
    dst = global_image * (mask[:, :, None].astype(global_image.dtype))
    while True:
        hsv = cv2.cvtColor(dst, cv2.COLOR_BGR2HSV)

        # считываем значения бегунков
        h1 = cv2.getTrackbarPos('h1', 'settings')
        s1 = cv2.getTrackbarPos('s1', 'settings')
        v1 = cv2.getTrackbarPos('v1', 'settings')
        h2 = cv2.getTrackbarPos('h2', 'settings')
        s2 = cv2.getTrackbarPos('s2', 'settings')
        v2 = cv2.getTrackbarPos('v2', 'settings')

        # формируем начальный и конечный цвет фильтра
        h_min = np.array((h1, s1, v1), np.uint8)
        h_max = np.array((h2, s2, v2), np.uint8)

        # накладываем фильтр на кадр в модели HSV
        thresh = cv2.inRange(hsv, h_min, h_max)

        cv2.imshow('result', thresh)
        mask = thresh != 0
        img0 = dst * (mask[:, :, None].astype(dst.dtype))
        cv2.imshow('result1', img0)
        ch = cv2.waitKey(5)
        if ch == 27:
            break
    cv2.destroyAllWindows()


def resize_image(input_image_path, output_image_path,
                 size):
    from PIL import Image
    original_image = Image.open(input_image_path)
    resized_image = original_image.resize(size)
    resized_image.save(output_image_path)
    return resized_image


if __name__ == "__main__":
    resize_image('3.jpg', '3.jpg', (640, 1000))
    chair = imread('3.jpg')

    color_trackbars(chair)
    cv2.waitKey()
